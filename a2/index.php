<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Hannah Brownsberger: The Makings of a Jack of all Trades">
		<meta name="author" content="Hannah Brownsberger">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong>
					(1) Create a mobile recipe app using Android Studio.
					(2) Inclue README.md with screenshots of the two pages in app.
					(3) Change the text color and background.
				</p>

				<h4>Bruchetta Recipe Page 1</h4>
				<img src="img/a2_2.png"  class="img-responsive center-block" alt="Bruchetta Recipe Page 1">

				<h4>Bruchetta Recipe Page 2</h4>
				<img src="img/a2_1.png" class="img-responsive center-block" alt="Bruchetta Recipe Page 2">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
