> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Hannah Brownsbegrer

### Assignment #2:

*Assignment Requirements:*

1. Create a mobile recipe app using Android Studio.
2. Inclue README.md with screenshots of the two pages in app.
3. Change the text color and background.

#### README.md file should include the following items:

* Screenshot of the Healthy Recipies Home Page.
* Screenshot of the Bruschetta Recipe Page.


#### Assignment Screenshots:

*Screenshots*

![A2 Image 1](img/a2_1.png)

*Screenshot of running java Hello*:

![A2 Image 2](img/a2_2.png)

*Screenshot of skillsets*

![Skillset 1 Screenshot](img/hb_ss1.png)

![Skillset 2 Screenshot](img/hb_ss2.png)

![Skillset 3 Screenshot](img/hb_ss3.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
