public class Main {
    public static void main(String[] args) {

        System.out.println("Created by Hannah Brownsberger");
        System.out.println("Program searches user-entered integers within an array of integers.");
        System.out.println("Creates an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");

        Methods.nestedStructure();
    }
}