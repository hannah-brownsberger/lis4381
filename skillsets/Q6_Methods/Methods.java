import java.util.*;

public class Methods {
    public static void getRequirements() {=;
    
        public static void getUserInput() {
            Scanner sc = new Scanner(System.in);

            System.out.println("Enter first name: ");

            System.out.print("\nEnter age: ");
            int age = sc.nextInt();

            myVoidMethod(name. age);
            System.out.println(myValueReturningMethod(name, age));
        };

        public static void myVoidMethod(String name, int age) {
            System.out.println("\nvoid method call: " + name + " is " + age);
        };

        public static String myValueReturningMethod(String name, int age) {

            String methodReturn = "value returning method call " + name + " is " + age;

            return methodReturn;
        }
    }
}