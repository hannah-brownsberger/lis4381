//Problem 

import java.util.Scanner; //this file we have to import from library

public class Methods
{

    //here is the first method that that inroduces people to the program. So, no object because it doesn't retun anything, just prints:
    public static void getRequirements()
    {
        System.out.println("Developer: Hannah Brownsberger");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do... while.");
        System.out.println();
        System.out.println("Note: Pretest loops: for, enhanced for, while. Post-test loop: do.. while.");
        System.out.println();

    } //end of getRequirements Method

    //now we do a method for the arrays!
    public static void arrayLoops(){
        //we are gonna declare and array and populate it once create it 
        //reminder that arrays have the square brackets 

                            //0      1       2      3        4      there's 5 array slots, but their index starts with 0
        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("for loop: "); //in the loops we create local variables to return 
        for(int i = 0; i<animals.length; i++){
            System.out.println(animals[i]);
        }//end of for loop

        //enhanced for loop is used with arrays where : means "in" so we create a variable that matches the type in the array. 
        System.out.println("\nenhanced for loop: ");
        for (String test : animals){ //this reads as for each string test in array animals
            System.out.println(test);
        }//end of enhanced for loop

        //now we create a  while loop where we declare variable before
        System.out.println("\nwhile loop: ");
        int i = 0;
        while(i < animals.length){
            System.out.println(animals[i]);
            i++;
        } //end of while loop 
        i = 0;
        //now we end with do... while loop aka post test loop!
        System.out.println("\ndo... while loop: ");
    
        do{
            System.out.println(animals[i]);
            i++;
        } while(i <animals.length); //end of do... while



    } //end of arrayLoops method


}//end of Methods