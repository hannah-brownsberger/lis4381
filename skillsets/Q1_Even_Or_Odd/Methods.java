import java.util.Scanner; //this file we have to import from library

public class Methods
{

public static void getRequirements()
{
System.out.println("Developer: Hannah Brownsberger");
System.out.println("This program evaluates integers as even or odd.");
System.out.println("Note: Program does *not* check for non-numeric characters.");

System.out.println();
} //end of getRequirements Method
public static void evaluateNumber()
{
//create variable and change it to become whatever the user inputs from scanner
int x = 0;
System.out.println("Enter integer: ");
Scanner sc = new Scanner(System.in);
x =sc.nextInt(); //basically calls for integer detected from scanner

//now an if else statement to figure out if it is an even or odd number
if (x % 2 == 0){
System.out.println(x + " is an even number!");
}
else{
System.out.println(x + " is an odd number!");
}

} //end of evaluateNumber method


} //end of methods