import java.util.Scanner;
import java.util.Random;

public class Methods 
{
    public static void getRequirements()
    {
        
        System.out.println("Print minimum and maximum integer vlaues.");
        System.out.println("Program prompts user to enter desired number of psydorandom-generated integers (min 1).");
        System.out.println("Use following loop structure: for, enhanced for, while, do... while.\n");
        System.out.println("Integer.MIN_VALUE =" + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);
        System.out.println();
    }
    public static int[] createArray()
    {
        Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        System.out.print("Enter desired number of pseudorandom integers (min 1):");
        arraySize = sc.nextInt();

        int yourArray[] = new int[arraySize];
        return yourArray;
    }
}