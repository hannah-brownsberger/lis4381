class Main 
{
    public static void main(String args[])
    {
        System.out.println("Project Creator: Hannah Brownsberger");
        Methods.getRequirements();
        int[] userArray = Methods.createArray();
        Methods.generatePseudoRandomNumbers(userArray);
    }
}