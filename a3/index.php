<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Hannah Brownsberger: The Makings of a Jack of all Trades">
		<meta name="author" content="Hannah Brownsberger">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong>
					(1) Course title, your name, assignment requirements, as per A1;
					(2) Screenshot of ERD;
					(3) Screenshot of running application’s first user interface;
					(4) Screenshot of running application’s second user interface;
					(5) Screenshots of 10 records for each table—use select * from each table;
					(6) Links to the following files:
					(a) a3.mwb
					(b) a3.sql
				</p>

				<h4>EERD<h4>
				<img src="img/EERD.png" class="img-responsive center-block" alt="EERD">

				<h4>Customer Table</h4>
				<img src="img/customertable.png" class="img-responsive center-block" alt="Customer Table">

				<h4>Petstore Table</h4>
				<img src="img/petstoretable.png" class="img-responsive center-block" alt="Petstore Table">

				<h4>Pet Table</h4>
				<img src="img/pettable.png" class="img-responsive center-block" alt="Pet Table">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
