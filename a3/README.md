> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Hannah Brownsberger

### Assignment 3 Requirements:

*Requirements:*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Screenshots of 10 records for each table—use select * from each table;
6. Links to the following files:
a. a3.mwb
b. a3.sql


#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of Running Application's first user interface
* Screenshot of running application's second user interface
* Screenshot of 10 records for each table
* Links to a3.mwb and a3.sql



#### Assignment Screenshots:

*Screenshot of EER Diagram*:

![EER Diagram Screenshot](img/EERD.png)

*Screenshot of petstore table*:

![Petstore Table](img/petstoretable.png)

*Screenshot of customer table*:

![Customer Table](img/customertable.png)

*Screenshot of pet table*:

![Pet Table](img/pettable.png)

####  Links:

*Links to MySQL Work*:

[Link to a3.mwb](doc)
[Link to a3.sql](doc)

*can't figure out how to make this link work, but it works in the docs file on this repo*