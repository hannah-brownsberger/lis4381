> This is my professional bitbucket that I have developed skills in Web Application Development.
>

# LIS 4831

## Hannah Brownsberger

### Assignment 5 Requirements:

*Requirements:*

1. Use A4 cloned files to create a add_petsore.php.
    - Add placeholder to each form id that gives the user an idea of how to enter their data.
2. Edit test_connection.php to connect to MySQL FSU account to access EERD from A3.
3. Add your MySQL data to display as a table.
4. Add new set of data to displayed MySQL chart on A5.

#### README.md file should include the following items:

* root index.php screenshot
* add_petstore.php (invalid) screenshot
* add_petstore_process.php (Failed Validation) screenshot
* add_petstore.php (valid) screenshot
* add_petstore_process.php (Passed Validation) screenshot


#### Assignment Screenshots:

*Screenshot of A5 from localhost*:

![a5](img/a5_mysql.png)

*Screenshot of Server Information*:

![server info](img/server_info.png)

*Screenshot of Validations (please see following text/zoom video)*:

![a5](img/validation_1.png)

> After tinkering with the format tonight, for some reason my add_petstore.php won't validate *at all*. I've looked over all of the code and I cannot find the issue. If we can meet to see if we can find the issue I would appreciate it. 
>


#### Notes: