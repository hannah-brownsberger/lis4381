> **This is my professional Bitbucket.**
>

# LIS4381

## Hannah Brownsberger

### Assignment 1 Requirements:

*Requirements:*

1. Distributed Version Control with Bitbucket and Git
2. Development Instalations
3. Chapter Questions (Ch 1 and 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Instalation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)


> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. pwd - Used to print the 'present working directory'

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/AMPPS.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/myfirstapp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/hannah-brownsberger/lis4381_demo/src/master/ "Bitbucket Station Locations")

