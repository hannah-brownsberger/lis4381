<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Hannah Brownsberger: The Makings of a Jack of all Trades">
		<meta name="author" content="Hannah Brownsberger">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong>
					(1) Distributed Version Control with Bitbucket and Git.
					(2) Development Instalations.
					(3) Chapter Questions (Ch 1 and 2).
				</p>

				<h4>Android Studio Installation</h4>
				<img src="img/myfirstapp.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Java Installation</h4>
				<img src="img/java.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/AMPPS.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
