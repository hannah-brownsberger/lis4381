# Mobile Web Application Development: LIS4381

## Hannah Brownsberger

### LIS4381 Course Description:
*This course focuses on concepts and best practices for developing and managing “mobile-first” technology projects. It covers processes and requirements for developing mobile web applications and principles for effective interface and user experience design. Students will also examine different issues and concerns that may influence the widespread adoption and implementation of mobile web applications.*

***Course Work Links***

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Download AMPPS
    - Download Java
    - Download Android Studio
    - Create 'Hello World!' App
    - Push Local respository to Bitbucket server
    - Provide read-only access and screenshots
        - Screenshots:
        - Main README.md
        - A1 README.md
        - RUnning Java
        - App 'Hello Hannah Brownsberger!'

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Provide screenshots of the two pages you have created as user activities via the android simulator.
    - Change the background and text color.

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create an EER Diagram per instructions.
    - Create an app to calculate the cost of concert tickets.
    - Save screenshots of tables, EER Diagram, and app part 1 and 2.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone assignment starter files
    - Open index.php and review the code
    - Create a favicon using your initials and place it in each assignment's main directiory.
    - In the end, you will have an online porfolio.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - TBD

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Reverse engineer example photos provided by the professor.
    - Learn how to create a launcher icon and display it in both activities.
    - Add border around image and button.
    - Add text shadow.
    - Complete questions.

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Open index.php and review code:
        - Suitably modify meta tags
        - Change title, navigation links, and header tags appropriately
        - See videos for complete development.
        - Turn off client-side validation
        - ***Be sure*** to include the following screenshots:
            * Carousel (Home page –include images, self-promotional links)
            * index.php
            * edit_petstore.php
            * Failed Validation
            * Passed Validation
            * Delete Record Prompt
            * Successfully Deleted Record
            * RSS Feed (Link to RSS feed of your choice)

***Notes***
>To update README.md on Git:
 - cd /c/Users/hbrow/repos/lis4381
 - 'git add .' / 'git commit -m "changed main README.md"' / git push
>
>To repos directory:
cd "c:\Program Files\Ampps\www\repos"
>

>***VOCABULARY***
>
>git init - This creates a new Git repository or can convert and existing project.
>
>git status - This lets you see the state of the working directory via what changes have been staged, have not, and which files aren't being tracked by Git.
>
>git add - This adds a change in the working directory to the staging area. It sends the pending change you're making to the staging area.
>
>git commit - This essentially takes a snapshot of the project's currently staged changes and it will not be changed by Git unless you explicitly make the changes. This screenshot is always saved to the local repository.
>
>git push - This uploads local repository content to a remote repository. This is how you transfer commits from local repo to remote repo. Pushing has the potential to overwrite changes, so be sure you're ready to push when you do.
>
>git fetch - This imports commits to local branches, pushing exports commoits to remote branches.
>
>git pull - This is used to fetch and download content from a remote repository and immerdiately update the local repository to match that content.
>
>staging area - This is a buffer between the working directory and the project history. 'git add' creates pending changes that would be sent here to be approved or rejected.
>
>cd - change directory
>
>pwd - print working directory
>
>ls - Shows information about files and the index and the working tree.
>
>Ctrl+cl - clear screen
>
>local repository - This is on your computer and changed through Git or Files.
>
>remote repository - This is what BitBucket is. Remote repositories are versions of your project that are hosted on the Internet or network somewhere.
>