> This is my professional bitbucket.
>

# LIS 4831

## Hannah Brownsberger

### Assignment 4 Requirements:

*Requirements:*

1. cd to local repo subdirectory
2. Clone assignment starter files provided
3. Edit subdirectories as needed then delete the clone
4. Open root index.php and edit home page carousel to reflect your designs.
5. Open a4 index.php and add a functional jQuery validation and regular expression for name, street, city, state, zip, phone, email, url, and ytd sales.
6. Submit screenshots of carousel, successful validation, and failed validation to A4 directory bitbucket.

#### README.md file should include the following items:

* Screenshot of my portal carousel
* Screenshot of my A4 with failed validation
* Screenshot of my A4 with a successful validation


#### Assignment Screenshots:

*Screenshot of LocalHost webpage home (with carousel)*:

![carousel](img/a4_carousel.png)

*Screenshot of A4 tab with a failed verification*:

![failed](img/a4_failed.png)

*Screenshot of A4 tab with a successful verification*:

![succeeded](img/a4_success.png)


#### Notes:
http://localhost/repos/lis4381/a1/index.php