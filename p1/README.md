> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Hannah Brownsberger

### Project 1:

*Objective:*

1. Reverse Engineer Photos
2. Learn How To Set Launcher Icon
3. Complete Questions

#### README.md file should include the following items:

* Screenshot of interface 1 functioning
* Screenshot of interface 2 functioning

#### Assignment Screenshots:



*Screenshot of Android Studio p1:


| Version 1 of Interface 1  | Version 2 of Interface 1 |
| ------------- | ------------- |
| ![Project 1 Interface 1 Version 1](img/p1_ss1_v1.png) | ![Project 1 Interface 1 Version 2](img/p1_ss1_v2.png)  |
| Interface 2 |  |
| ![Project 1 Interface 2](img/p1_ss2.png)  |    |

>I had issues with getting my launcher icon to display my logo properly and ended up not using it, instead just trying to get it to format corrctly. Also, it may be hard to see, but I used a transparent image as the border around my main image and around the button.

