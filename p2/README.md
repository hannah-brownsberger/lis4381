> This is my professional bitbucket that I have developed skills in Web Application Development.
>

# LIS 4831

## Hannah Brownsberger

### Project 2 Requirements:

*Requirements:*

1. Open index.php and review code:
- Suitably modify meta tags
- Change title, navigation links, and header tags appropriately
- See videos for complete development.
- Turn off client-side validation by commenting out the following code:
<?php //include_once("../js/include_js.php"); ?>
e. Add server-side validation and regular expressions-- as per the database entity attribute
requirements
- ***Be sure*** to include the following screenshots (see below):
- Before *and* after successful edit
- Failed validation
- Delete promp

#### README.md file should include the following items:

* Carousel (Home page –include images, self-promotional links)
* index.php
* edit_petstore.php
* Failed Validation
* Passed Validation
* Delete Record Prompt
* Successfully Deleted Record
* RSS Feed (Link to RSS feed of your choice)



#### Assignment Screenshots:

*Screenshot of Carousel*:

![carousel](img/carousel.jpg)

*Screenshot Database table*:

![database](img/mysql.png)

*Screenshot of edit_petstore*:

![edit_petstore](img/edit_petstore.png)

*Screenshot of failed validation*:

![failed validation](img/failed_validation.png)

*Screenshot of passed validation*:

***N/A***

*Screenshot of delete record prompt*:

![prompt](img/delete_file_prompt.png)

*Screenshot of successfully deleted record*:

***N/A***

*Screenshot of RSS Feed*:

![rss](img/rss.png)




#### Notes: