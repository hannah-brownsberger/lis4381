<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Hannah Brownsberger: The Makings of a Jack of all Trades">
		<meta name="author" content="Hannah Brownsberger">
		<link rel="icon" href="favicon.ico">

		<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #666;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="1000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    					<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/banner.png) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>My Small Business</h3>
										<p class="lead">Baba Yaga's Bits and Bobs is my small business I began in 2019 creating artisan dice for Tabletop Role-Playing Games such as Dungeons and Dragons.</p>
						</div>
					</div>
				</div>

								<div class="item">
									<h2>Slide 2</h2>
									<div class="carousel-caption">
										<h3>Leadership in Technology</h3>
										<p> The TalTech Conference was a techology conference I helped direct in Tallahasse in November 2021. This is a photo of some of the TalTech team.</p> 
										<img src="img/taltech.jpg" alt="Slide 2">
									</div>
								</div>	 

								<div class="item">
									<h2>Slide 3</h2>
									<div class="carousel-caption">
										<h3>Bitbucket</h3>
								<!--	<p> The TalTech Conference was a techology conference I helped direct in Tallahasse in November 2021. This is a photo of some of the TalTech team.</p> -->
										<img src="img/bitbucket.png" alt="Slide 3">
									</div>
								</div>	 
		 					
						</div>	                   

						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
